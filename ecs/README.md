# Tikan-ECS
This crate provides an engine agnostic ECS crate which is build to manage more or less physical worlds.
It will provide some easy api to query for specific entities based on components they have, or for building acceleration 
structures for objects with specific components.
