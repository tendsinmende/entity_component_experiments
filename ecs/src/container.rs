/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use crate::*;

use std::collections::{BTreeMap};




pub enum ContainerType<C>{
    Tree(BTreeContainer<C>),
    Vec(VecContainer<C>),
}

impl<C: Send + Sync + 'static> ContainerType<C>{
    pub fn store(&self, entity: &EntityId, component: C){
        match self{
            ContainerType::Tree(t) => t.store(entity, component),
            ContainerType::Vec(t) => t.store(entity, component),
        }
    }
    pub fn remove(&mut self, entity: &EntityId){
        match self{
            ContainerType::Tree(t) => t.remove(entity),
            ContainerType::Vec(t) => t.remove(entity),
        }
    }
    pub fn get(&self, entity: &EntityId) -> Option<Arc<RwLock<C>>>{
        match self{
            ContainerType::Tree(t) => t.get(entity),
            ContainerType::Vec(t) => t.get(entity),
        }
    }
}

pub struct BTreeContainer<C>{
    container: RwLock<BTreeMap<EntityId, Arc<RwLock<C>>>>,
}

impl<C: Send + Sync + 'static> BTreeContainer<C>{
    pub fn new() -> Self{
        BTreeContainer{
            container: RwLock::new(BTreeMap::new())
        }
    }

    fn store(&self, entity: &EntityId, component: C){
        self.container.write().expect("Failed to write component!").insert(*entity, Arc::new(RwLock::new(component)));
    }
    fn remove(&mut self, entity: &EntityId){
        self.container.write().expect("Failed to remove component").remove(&entity);
    }
    fn get(&self, entity: &EntityId) -> Option<Arc<RwLock<C>>>{
        if let Some(comp) = self.container.read().expect("Failed to read component!").get(entity){
            return Some(comp.clone());
        }

        None
    }
}


pub struct VecContainer<C>{
    container: RwLock<Vec<(EntityId, Arc<RwLock<C>>)>>,
}

impl<C: Send + Sync + 'static> VecContainer<C>{
    pub fn new() -> Self{
        VecContainer{
            container: RwLock::new(Vec::new())
        }
    }

    fn store(&self, entity: &EntityId, component: C){
        self.container.write().expect("Failed to write component!").push((*entity, Arc::new(RwLock::new(component))));
    }
    fn remove(&mut self, entity: &EntityId){
        //Find the index for removing
        let idx = if let Some(i) = self.find(entity){
            i
        }else{
            return;
        };
        
        let _old = self.container.write().expect("Failed to remove component").remove(idx);
    }
    fn get(&self, entity: &EntityId) -> Option<Arc<RwLock<C>>>{

        let idx = if let Some(i) = self.find(entity){
            i
        }else{
            return None;
        };
        
        
        let lock = self.container.read().expect("Failed to read vec container");
        let comp = &lock.get(idx).expect("Failed to get component at id").1;

        
        Some(comp.clone())
    }

    fn find(&self, entity: &EntityId) -> Option<usize>{
        for (i, (e, _c)) in self.container.read().expect("Failed to read Vec container").iter().enumerate(){
            if e == entity{
                return Some(i);
            }
        }

        None
    }
}


//TODO find a way to implement the containers with a generic that gets down cast to from a Arc<Any> to an Arc<Container<C>>
