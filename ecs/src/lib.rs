/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::sync::RwLock;
use std::sync::{Arc};
use std::collections::{BTreeMap};
use std::any::{TypeId, Any};

///Contains different types of how a component can be stored
pub mod container;
use crate::container::*;

///The internal id of an Entity
pub type EntityId = u32;

///A System can manipulate data. It can be implemented for struct if you want to save special data,
/// or you can use a simple closure as a system.
pub trait System{
    fn run(&self, entity: &Entity, components: &World);
}

///Implements a `system` struct with the given arguments.
/// # Example
///```rust ignore
///struct Position{
///    x: f32,
///    y: f32,
///}
///
///struct Velocity{
///    x: f32,
///    y: f32
///}
///
///
///system!(Physics, |position: Write<Position>, velo: Read<Velo>|{ //Note arguments always have to be Arc<T>
///    *position.x += velo.x;
///    *position.y += velo.y;
///});
///
///fn main(){
///    let mut world = World::new();
///    world.add_system(Physics::new());
///
///}
///```
#[macro_export]
macro_rules! system{
    ($system:ident, | $($arg:ident: $rwq:ident<$arg_ty:ty>),* | $run:block) =>(
        pub struct $system{}
        impl $system{
            pub fn new() -> $system{
                $system{}
            }
        }

        impl System for $system{
            fn run(&self, entity: &Entity, components: &World){
                use std::sync::{RwLockReadGuard, RwLockWriteGuard};
                //Find all components needed
                $(
                    to_system_var!($arg: $rwq $arg_ty, components, entity);
                )*
                //Run system code
                $run
            }
        }
    )
}


#[macro_export]
macro_rules! to_system_var{
    ($arg:ident: Read $arg_ty:ty, $comp:ident, $ent:ident ) => (
        let lock = if let Some(a) = $comp.get_component::<$arg_ty>($ent){
            a
        }else{
            return
        };
            
        let $arg = if let Ok(inner) = lock.read(){
            inner
        }else{
            return;
        };
    );
    ($arg:ident: Write $arg_ty:ty, $comp:ident, $ent:ident ) => (
        let lock = if let Some(a) = $comp.get_component::<$arg_ty>($ent){
            a
        }else{
            return
        };
            
        let mut $arg = if let Ok(inner) = lock.write(){
            inner
        }else{
            return;
        };
    );
}


///An Entity represents a "thing" within a `World`. Components can be attached to an entity.
/// If a system is registered for a world, it gets executed on every entity that has the required components attached.
#[derive(Copy,Clone, Debug)]
pub struct Entity{
    id: EntityId,
}
impl Entity{
    pub fn id(&self) ->&EntityId{
        &self.id
    }
}

pub struct World{
    entities: Vec<Entity>,
    ///Per component type stores a list of actual components.
    components: BTreeMap<TypeId, Arc<dyn Any + Send + Sync + 'static>>,
    systems: Vec<Arc<dyn System>>,

    last_entity_id: EntityId,
}

impl World{
    ///Creates a new, empty world
    pub fn new() -> Self{
        World{
            entities: Vec::new(),
            components: BTreeMap::new(),
            systems: Vec::new(),

            last_entity_id: 0,
        }
    }

    ///Allocates a new entity within this world
    pub fn new_entity(&mut self) -> Entity{
        let entity_id = self.last_entity_id;
        self.last_entity_id += 1;
        let entity = Entity{id: entity_id};
        self.entities.push(entity);

        entity
    }

    ///Executes all systems on the world
    pub fn update(&mut self){
        println!("UPD");
        for sys in self.systems.iter(){
            for entity in self.entities.iter(){
                sys.run(&entity, &self);
            }
        }
    }

    ///Adds a system to the world. Have a look at the `system!` macro to see how those are implemented.
    /// Note that a registered system gets executed on each entity that has the required components. That
    /// might change in the future.
    pub fn add_system<F: 'static>(&mut self, sys: F) where F: System
    {
        self.systems.push(
            Arc::new(sys)
        );
    }

    ///Returns an RwLock to a component from type `C` for an entity.
    ///
    ///Returns None if:
    /// - No component from type `C` was added for `entity`
    pub fn get_component<'a, C: Send + Sync + 'static>(&'a self, entity: &Entity) -> Option<Arc<RwLock<C>>>{
        if let Some(component_map) = self.components.get(&TypeId::of::<C>()){
            if let Some(c) = component_map.downcast_ref::<ContainerType<C>>(){
                return c.get(entity.id());
            }
        }

        None
    }

    ///Adds a component of type C to the entity. Fails if:
    /// - If no component of type C was `register()` on self
    /// - If the system somehow else failed to find the container.
    pub fn add_component<C: Send + Sync + 'static>(&mut self, entity: &Entity, component: C) -> Result<(), String>{
        if let Some(component_map) = self.components.get_mut(&TypeId::of::<C>()){
            if let Some(c) = component_map.downcast_ref::<ContainerType<C>>(){
                c.store(entity.id(), component);
                Ok(())
            }else{
                Err(format!("Could not downcast from Arc<Any> to Arc<ContainerType<{}>>", stringify!(C)))
            }
        }else{
            Err(format!("No Container registered for type {}", stringify!(C)))
        }
    }
    
    pub fn register<C: Send+Sync+'static>(&mut self, container: ContainerType<C>){
        self.components.insert(TypeId::of::<C>(), Arc::new(container));
    }
}
