use ecs::*;

use std::time::*;

pub const NUM_ENTITIES: usize = 10_000;

#[derive(Debug)]
struct Position{
    x: f32,
    y: f32,
}

#[derive(Debug)]
struct Velocity{
    x: f32,
    y: f32
}

struct Name{
    name: String
}


system!(Physics, |position: Write<Position>, velo: Read<Velocity>|{

    position.x += velo.x;
    position.y += velo.y;
});


fn main(){
    let mut world = World::new();

    world.register::<Position>(container::ContainerType::Tree(container::BTreeContainer::new()));
    world.register::<Velocity>(container::ContainerType::Tree(container::BTreeContainer::new()));
    //world.register::<Name>(container::ContainerType::Tree(container::BTreeContainer::new()));

    let mut entities = Vec::with_capacity(NUM_ENTITIES);


    let build_start = Instant::now();
    for _i in 0..(NUM_ENTITIES / 10 * 9){
        let e = world.new_entity();
        world.add_component(&e, Position{x: 1.0, y: 1.0}).expect("Failed to add position");
        entities.push(e);
    }

    for _i in 0..(NUM_ENTITIES / 10 * 1){
        let e = world.new_entity();
        world.add_component(&e, Position{x: 1.0, y: 1.0}).expect("Failed to add position");
        world.add_component(&e, Velocity{x: 1.0, y: -1.0}).expect("Failed to add Velocity");
        entities.push(e);
    }    
    world.add_system(Physics::new());
    
    let btime = build_start.elapsed();
    println!("Build Needed {}sec / {}msec / {}nsec",
             btime.as_secs(), btime.as_millis(), btime.as_nanos()
    );

    println!("Which is {}msec per inst", btime.as_millis() as f64 / 10_000.0);    

    return;
    loop {
        let upd_start = Instant::now();
        world.update();
        let time = upd_start.elapsed();
        println!("Needed {}sec / {}msec / {}nsec",
                 time.as_secs(), time.as_millis(), time.as_nanos()
        );

        println!("Which is {}msec / {}ns per inst", time.as_millis() as f64 / 10_000.0, time.as_nanos() as f64 / 10_000.0);    
    }
    
}
