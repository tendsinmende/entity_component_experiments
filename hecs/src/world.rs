/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::*;

use std::collections::BTreeMap;
use std::sync::{Arc, RwLock};
use std::any::*;

/// A `World` which manages all components for entities of this world.
pub struct World{
    ///Stores the component container sorted by the C in `Container<C>`
    components: BTreeMap<TypeId, Arc<dyn Any + Send + Sync + 'static>>,
    entities: Vec<Arc<Entity>>,
    free_ids: Vec<usize>,
}

impl World{
    pub fn new() -> Self{
        World{
            components: BTreeMap::new(),
            entities: Vec::new(),
            free_ids: Vec::new(),
        }
    }

    ///Creates a valid entity for this world. Returns the Id which is used to
    /// access its content.
    pub fn alloc(&mut self) -> Arc<Entity>{
        let e_id = if self.free_ids.len() > 0{
            self.free_ids.pop().expect("Failed to pop entity Id, but there should be one!")
        }else{
            self.free_ids.len()
        };

        let entity = Entity::new(e_id);
        self.entities.insert(e_id, entity.clone());
        entity
    }
    
    ///Returns the container for type `C`, if there is one.
    /// # Safety
    /// If you `.write()` on this container no systems or service can write on it which might block
    /// a `world.update()` until you release the lock.
    pub fn get_container<C: Send + Sync + 'static>(&self) -> Option<Arc<RwLock<Container<C>>>>{
        if let Some(cont) = self.components.get(&TypeId::of::<C>()){
            if let Ok(inner) = cont.clone().downcast::<RwLock<Container<C>>>(){
                return Some(inner.clone());
            }
        }

        None
    }

    ///Same as `get_container()`, but instead using the type id directly.
    pub fn get_container_by_id<C: Send + Sync + 'static>(&self, id: TypeId) -> Option<Arc<RwLock<Container<C>>>>{
        if let Some(cont) = self.components.get(&id){
            if let Ok(inner) = cont.clone().downcast::<RwLock<Container<C>>>(){
                return Some(inner.clone());
            }
        }

        None
    }

    ///Adds a component from type `C` to the entity
    pub fn add_component<C: Send + Sync + 'static>(&mut self, entity: &Arc<Entity>, component: C){
        //First add component to container then register within the entity
        let container = if let Some(container) = self.get_container::<C>(){
            container
        }else{
            //Add a container for this type
            self.components.insert(TypeId::of::<C>(), Arc::new(RwLock::new(Container::<C>::new())));
            //Now there has to be one
            self.get_container::<C>().expect("Failed to get just added container!")
        };

        let container_resource_id = container.write().expect("Failed to add resource for entity!").store(component);
        //Now register this id within the entity.
        entity.add_component(TypeId::of::<C>(), container_resource_id);
    }
}
