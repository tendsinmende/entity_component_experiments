/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::sync::RwLockWriteGuard;
use std::sync::RwLockReadGuard;
use crate::*;
use std::sync::RwLock;
use std::sync::Arc;
use std::any::TypeId;

///An Id that is used to reference entities within a `World`.
pub type EntityId = usize;

#[derive(Debug)]
pub struct Entity{
    id: EntityId,
    ///Contains for each type of component the index within the relative component container.
    components: RwLock<Vec<(TypeId, usize)>>,
    children: RwLock<Vec<Arc<Entity>>>,
}

impl Entity{

    pub fn new(id: EntityId) -> Arc<Self>{
        Arc::new(Entity{
            id,
            components: RwLock::new(Vec::new()),
            children: RwLock::new(Vec::new()),
        })
    }
    
    pub fn id(&self) -> EntityId{
        self.id
    }
    ///If a component of type `C` was registered for this entity, it will return the index into the container for type `C`
    /// to retrieve it.
    pub fn component_id(&self, component_type: TypeId) -> Option<usize>{
        for (ty, id) in self.components.read().expect("Failed to read components!").iter(){
            if *ty == component_type{
                return Some(*id);
            }
        }
        None
    }

    ///Same as `component_id()` but from type parameter
    pub fn get_component<C: Send+Sync+'static>(&self) -> Option<usize>{
        let tyid = TypeId::of::<C>();
        self.component_id(tyid)
    }

    pub fn add_component(&self, type_id: TypeId, index: usize){
        self.components.write().expect("Failed to insert new component").push((type_id, index));
    }

    pub fn add_child(&self, child: Arc<Entity>){
        self.children.write().expect("Could not push child into entity").push(child);
    }
    
    ///Returns all direct children of this entity
    pub fn children(&self) -> Vec<Arc<Entity>>{
        self.children.read().expect("Failed to read children!").clone()
    }
}
