/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::entity::Entity;
use std::sync::Arc;
use std::any::TypeId;

///Manages a list of components from type `C`.
pub struct Container<C>{
    components: Vec<C>,
    ///When a entity is deleted, tracks reusable indices for new allocations
    free_indices: Vec<usize>,
}

impl<C: Send + Sync + 'static> Container<C>{
    pub fn new() -> Self{
        Container{
            components: Vec::new(),
            free_indices: Vec::new()
        }
    }

    ///Stores a new components, returns the index at which it can be retrieved.
    pub fn store(&mut self, new: C) -> usize{
        let indice = if self.free_indices.len() > 0{
            self.free_indices.pop().expect("Failed to pop indice while len > 0, should not happen")
        }else{
            self.components.len()
        };

        //Store the new component
        self.components.insert(indice, new);

        indice
    }

    pub fn get(&self, idx: usize) -> Option<&C>{
        self.components.get(idx)
    }

    pub fn get_mut(&mut self, idx: usize) -> Option<&mut C>{
        self.components.get_mut(idx)
    }

    pub fn from_entity(&self, entity: &Arc<Entity>) -> Option<&C>{
        let idx = if let Some(i) = entity.get_component::<C>(){
            i
        }else{
            return None;
        };

        self.get(idx)
    }

    pub fn from_entity_mut(&mut self, entity: &Arc<Entity>) -> Option<&mut C>{
        let idx = if let Some(i) = entity.get_component::<C>(){
            i
        }else{
            return None;
        };

        self.get_mut(idx)
    }
    
    pub fn remove(&mut self, idx: usize){
        self.free_indices.push(idx);
    }

    pub fn container_type(&self) -> TypeId{
        TypeId::of::<C>()
    }
}

