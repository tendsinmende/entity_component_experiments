use std::sync::*;
use hecs::*;
use std::time::Instant;



pub const NUM_ENTITIES: usize = 1_000_000;

#[derive(Debug)]
struct Pos{
    x: f32,
    y: f32
}

struct Vel{
    x: f32,
    y: f32
}

#[derive(Debug)]
struct Name{
    name: String
}
impl Name{
    pub fn new(name: &str) -> Self{
        Name{
            name: name.to_string()
        }
    }
}




fn physics_sys(root_nodes: &[Arc<Entity>], world: &mut World){

    let pos_container = world.get_container::<Pos>().expect("Failed to get pos container!");
    let mut posc = pos_container.write().expect("Failed to write to pos container!");
    let velo_container = world.get_container::<Vel>().expect("Failed to get vel container!");
    let veloc = velo_container.read().expect("Failed to read from vel container!");
    
    for rnode in root_nodes.iter(){
        sub_phys(None, rnode, &mut posc, &veloc);
    }
}

fn sub_phys(_parent: Option<&Arc<Entity>>, node: &Arc<Entity>, pos_container: &mut Container<Pos>, velo_container: &Container<Vel>){

    let pos = if let Some(p) = pos_container.from_entity_mut(&node){
        p
    }else{
        return;
    };

    let vel = if let Some(v) = velo_container.from_entity(&node){
        v
    }else{
        return;
    };

    pos.x += vel.x;
    pos.y += vel.y;
    
    //println!("NewPos: {:?}", pos_container.from_entity_mut(&node).unwrap());
    
    for c in node.children(){
        sub_phys(Some(&node), &c, pos_container, velo_container);
    }
}

fn main(){
    let mut world = World::new();

    let mut entities = Vec::with_capacity(NUM_ENTITIES);

    let build_start = Instant::now();

    for i in 0..(NUM_ENTITIES / 10 * 9){
        let e = world.alloc();
        world.add_component(&e, Pos{
            x: 24.0,
            y: -10.0
        });

        entities.push(e);
    }

    for i in 0..(NUM_ENTITIES / 10 * 1){
        let e = world.alloc();
        world.add_component(&e, Pos{
            x: 24.0,
            y: -10.0
        }); 
        world.add_component(&e, Vel{
            x: 1.0,
            y: -1.0
        });

        entities.push(e);
    }

    let btime = build_start.elapsed();
    println!("Build Needed {}sec / {}msec / {}nsec",
             btime.as_secs(), btime.as_millis(), btime.as_nanos()
    );

    println!("Which is {}msec per inst", btime.as_millis() as f64 / 10_000.0);    
    
    loop{
        let upd_start = Instant::now();

        println!("=====");
        physics_sys(&entities, &mut world);
        let time = upd_start.elapsed();

        println!("Needed {}sec / {}msec / {}nsec",
                 time.as_secs(), time.as_millis(), time.as_nanos()
        );

        println!("Which is {}msec / {}ns per inst", time.as_millis() as f64 / 10_000.0, time.as_nanos() as f64 / 10_000.0);    
    }
}
