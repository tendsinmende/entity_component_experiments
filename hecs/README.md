# Tikan-Hecs

A "hierarchic" entity component system.

The basic idea is similar to a normal ECS. There are "entities", each entity can container several components like position, velocity, name etc.
Since each type of component can be grouped together in memory this system can be made really fast when it comes to "systems". Those systems take a set of components and change
them based on functions/rules. This way it is really easy to express something like: "Move each entity that has a position, velocity and a name".

# The "H" in HECS
My idea now is to change the "entity" part of ECS in a way that one can build trees from entities.
When being updated an child entity could inherit for instance the position of its parent etc.
This will be archived by modifying what an entity is. Usually those are implemented by just an Id which get de-referenced to concrete components.

In this case components are still organized in big component list (one list per component type). However, this time the index to an component is stored directly 
within the custom entity-structs. In that way a entity can update it self quiet fast by just indexing into the array.

Systems are currently not implemented as any trait or something. You just write one or more functions depending on how you want to update your entities.
The following is an example of a `World` with 1000 entities that contain a `Pos` and `Vel` component, as well as 9000 entities that only contain a `Pos` component.

The physics system "moves" entities if they have a world and pos component. It would also update its children in order.

``` rust
struct Pos{
    x: f32,
    y: f32
}

struct Vel{
    x: f32,
    y: f32
}

//Updates the given root nodes and their children
fn physics_sys(root_nodes: &[Arc<Entity>], world: &mut World){

    let pos_container = world.get_container::<Pos>().expect("Failed to get pos container!");
    let mut posc = pos_container.write().expect("Failed to write to pos container!");
    let velo_container = world.get_container::<Vel>().expect("Failed to get vel container!");
    let veloc = velo_container.read().expect("Failed to read from vel container!");
    
    for rnode in root_nodes.iter(){
        sub_phys(None, rnode, &mut posc, &veloc);
    }
}

//recursive entity tree traversal
fn sub_phys(_parent: Option<&Arc<Entity>>, node: &Arc<Entity>, pos_container: &mut Container<Pos>, velo_container: &Container<Vel>){

    let pos = if let Some(p) = pos_container.from_entity_mut(&node){
        p
    }else{
        return;
    };

    let vel = if let Some(v) = velo_container.from_entity(&node){
        v
    }else{
        return;
    };

    pos.x += vel.x;
    pos.y += vel.y;
        
    for c in node.children(){
        sub_phys(Some(&node), &c, pos_container, velo_container);
    }
}

fn main(){
    let mut world = World::new();

    let mut entities = Vec::with_capacity(10_000);

    for i in 0..9000{
        let e = world.alloc();
        world.add_component(&e, Pos{
            x: 24.0,
            y: -10.0
        });

        entities.push(e);
    }

    for i in 0..1000{
        let e = world.alloc();
        world.add_component(&e, Pos{
            x: 24.0,
            y: -10.0
        }); 
        world.add_component(&e, Vel{
            x: 1.0,
            y: -1.0
        });

        entities.push(e);
    }

    loop{
        physics_sys(&entities, &mut world);
    }
}
```

