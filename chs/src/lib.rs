/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::any::*;
use std::sync::{Arc,RwLock, Weak};

pub trait Merge{
    //Should merge two component types with common sense. For instance two Positions get added together etc.
    fn merge(&self, other: &Self) -> Self;
}

///A component that can hold any kind of data.
pub struct Component{
    //The inner component type wrapped away
    inner: (TypeId, Arc<dyn Any + Send + Sync + 'static>),
    children: RwLock<Vec<Arc<Component>>>,
    parent: Weak<Component>,
}

impl Component{
    ///Creates a new, independent component with no parent. If you want to attach a  new
    /// component to a component, use `attach()` on the parent component.
    pub fn new<C: Send + Sync + 'static>(element: C) -> Arc<Self>{
        Arc::new(Component{
            inner: (TypeId::of::<C>(), Arc::new(RwLock::new(element))),
            children: RwLock::new(Vec::new()),
            parent: Weak::new(), //Weak pointer to nothing
        })
    }

    ///Searches for a component of type `C` upward that tree.
    /// # Example
    /// If you have a tree of an person component. The person has a position, and attached to that position is a mesh and a camera. You can use `get_up()` on the camera to retrieve the
    /// location at which the camera should be positioned.
    /// # Safety
    /// If there are numerous candidates upward from that tree (in the case of our example there could be an offset position between the character-position and the camera to offset the camera behind the person)
    /// the returned value is always the nearest. However, you could also use `get_up_merge()` if the type `C` implements the `Merge` trait.
    pub fn get_up<C: Send + Sync + 'static>(&self) -> Option<Arc<RwLock<C>>>{
        //First check if we can use self's value, else check parents
        if self.inner.0 == TypeId::of::<C>(){
            if let Ok(inner_comp) = self.inner.1.clone().downcast::<RwLock<C>>(){
                return Some(inner_comp);
            }
        }
        //Check parents
        if let Some(p) = self.parent.upgrade(){
            p.get_up::<C>()
        }else{
            None
        }
    }

    ///Variation of `get_up()` check its documentation for more details.
    pub fn get_up_merge<C: Merge + Send + Sync + 'static>(&self) -> Option<Arc<RwLock<C>>>{
        //Check parents
        let parents_val = if let Some(p) = self.parent.upgrade(){
            p.get_up::<C>()
        }else{
            None
        };

        let selfs_val = if self.inner.0 == TypeId::of::<C>(){
            if let Ok(inner_comp) = self.inner.1.clone().downcast::<RwLock<C>>(){
                Some(inner_comp)
            }else{
                None
            }
        }else{
            None
        };

        match (parents_val, selfs_val){
            (Some(p), Some(s)) => {
                //Merge
                let merged_val = p.read().expect("Failed to read parent value while merging")
                    .merge(&s.read().expect("Failed to read selfs value while merging")); 
                Some(Arc::new(RwLock::new(merged_val)))
            },
            (None, Some(s)) => Some(s),
            (Some(p), None) => Some(p),
            (None, None) => None
        }
    }

    ///Searches the tree downwards from `self`.
    ///Similar to the `get_up` method values are going to be replaced with the "nearer" one if there are two candidates.
    /// Since there could be several candidates on one level, simply the first one is used.
    pub fn get_down<C: Send + Sync + 'static>(&self) -> Option<Arc<RwLock<C>>>{
        //First check if we can use self's value, else check children
        if self.inner.0 == TypeId::of::<C>(){
            if let Ok(inner_comp) = self.inner.1.clone().downcast::<RwLock<C>>(){
                return Some(inner_comp);
            }
        }

        for c in self.children.read().expect("Failed to read children!").iter(){
            let res = c.get_down::<C>();
            if res.is_some(){
                return res;
            }
        }

        None
    }

    ///Returns the inner component data if it is from type `C`
    pub fn get<C: Send + Sync + 'static>(&self) -> Option<Arc<RwLock<C>>>{
        if self.inner.0 == TypeId::of::<C>(){
            if let Ok(inner_comp) = self.inner.1.clone().downcast::<RwLock<C>>(){
                return Some(inner_comp);
            }
        }

        None
    }

}

pub trait AbstractComponent{
    ///Adds a component of type C to selfs children, returns a Pointer to this new component
    fn add_component<C: Send + Sync + 'static>(&self, element: C) -> Self;
}

impl AbstractComponent for Arc<Component>{
    ///Adds a component of type C to selfs children, returns a Pointer to this new component
    fn add_component<C: Send + Sync + 'static>(&self, element: C) -> Self{
        let new_component = Arc::new(Component{
            inner: (TypeId::of::<C>(), Arc::new(RwLock::new(element))),
            children: RwLock::new(Vec::new()),
            parent: Arc::downgrade(self), //Weak pointer to nothing
        });

        self.children.write().expect("Failed to write to children!").push(new_component.clone());
        new_component
    }    
}


