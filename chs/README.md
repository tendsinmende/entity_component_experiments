# Tikan Component Hierarchy System
A component hierarchy system is an from ecs systems inspired tree of components.
Each component stores some data. Systems can be executed on any part of a component tree. Through `get` methods it is possible to
retrieve component data of other components within that tree.

The main difference to traditional ECS is that the user manages "Entities" (in this case trees of components) himself. There is no abstract "World".

An implementation detail is in this case that the data is directly managed within components. So there is also no context which manages similar data in big lists.
This is around 40% slower then the context approach, however the added value is that the trees can easily be shared across threads.

# Position Updates based on velocity example

``` rust

#[derive(Debug)]
struct Pos{
    x: f32,
    y: f32
}

struct Vel{
    x: f32,
    y: f32
}

#[derive(Debug)]
struct Name{
    name: String
}
impl Name{
    pub fn new(name: &str) -> Self{
        Name{
            name: name.to_string()
        }
    }
}


pub fn physics(component: &Arc<Component>){
    let pos = if let Some(p) = component.get_down::<Pos>(){
        p
    }else{
        return;
    };

    let vel = if let Some(v) = component.get_down::<Vel>(){
        v
    }else{
        return;
    };

    let mut rw_pos = pos.write().unwrap();
    let r_vel = vel.read().unwrap();

    rw_pos.x += r_vel.x;
    rw_pos.y += r_vel.y;
        
}


fn main(){

    let mut  entities = Vec::with_capacity(10_000);
    let build_start = Instant::now();

    
    for _i in 0..67_500{
        let e = Component::new(Name::new("Entity!"));
        e.add_component(Pos{
            x: 24.0,
            y: -10.0
        });

        entities.push(e);
    }

    for _i in 0..7_500{
        let e = Component::new(Name::new("Entity!"));
        e.add_component(Pos{
            x: 24.0,
            y: -10.0
        }); 
        e.add_component(Vel{
            x: 1.0,
            y: -1.0
        });

        entities.push(e);
    }

	for e in entities.iter(){
		physics(e);
	}

}
```
