/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


pub const NUM_ENTITIES: usize = 1_000_000;

use std::sync::*;
use chs::*;
use std::time::Instant;

#[derive(Debug)]
struct Pos{
    x: f32,
    y: f32
}

struct Vel{
    x: f32,
    y: f32
}

#[derive(Debug)]
struct Name{
    name: String
}
impl Name{
    pub fn new(name: &str) -> Self{
        Name{
            name: name.to_string()
        }
    }
}


pub fn physics(component: &Arc<Component>){
    let pos = if let Some(p) = component.get_down::<Pos>(){
        p
    }else{
        return;
    };

    let vel = if let Some(v) = component.get_down::<Vel>(){
        v
    }else{
        return;
    };

    let mut rw_pos = pos.write().unwrap();
    let r_vel = vel.read().unwrap();

    rw_pos.x += r_vel.x;
    rw_pos.y += r_vel.y;
        
}


fn main(){

    let mut  entities = Vec::with_capacity(NUM_ENTITIES);
    let build_start = Instant::now();

    
    for _i in 0..(NUM_ENTITIES / 10 * 9){
        let e = Component::new(Name::new("Entity!"));
        e.add_component(Pos{
            x: 24.0,
            y: -10.0
        });

        entities.push(e);
    }

    for _i in 0..(NUM_ENTITIES / 10 * 1){
        let e = Component::new(Name::new("Entity!"));
        e.add_component(Pos{
            x: 24.0,
            y: -10.0
        }); 
        e.add_component(Vel{
            x: 1.0,
            y: -1.0
        });

        entities.push(e);
    }

    let btime = build_start.elapsed();
    println!("Build Needed {}sec / {}msec / {}nsec",
             btime.as_secs(), btime.as_millis(), btime.as_nanos()
    );

    println!("Which is {}msec per inst", btime.as_millis() as f64 / 10_000.0);    

    loop{
        let upd_start = Instant::now();

        println!("=====");
        for e in entities.iter(){
            physics(e);
        }

        
        let time = upd_start.elapsed();

        println!("Needed {}sec / {}msec / {}nsec",
                 time.as_secs(), time.as_millis(), time.as_nanos()
        );

        println!("Which is {}msec / {}ns per inst", time.as_millis() as f64 / 10_000.0, time.as_nanos() as f64 / 10_000.0);

        //println!("New pos: {:?}", entities[9000].get_down::<Pos>().unwrap());
        
    }

}
