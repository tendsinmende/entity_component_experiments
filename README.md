# ECS experiments
In this repository I collected my ecs experiments I created for [tikan](https://gitlab.com/Siebencorgie/tikan).

I settled for the CHS (Component Hierarchy System) which gives most control to the user, is fast and easy to share across threads. It will be further developed in that repository under the name `tikan-chs`.

# Performance Figures
## Running position updates on "some" entities:

![](figures/updating.png "Updating")

## Inserting entities
![](figures/insert_all.png "Inserting")


## Inserting entities without the HECS
![](figures/insert_no_hecs.png "Inserting without HECS")
