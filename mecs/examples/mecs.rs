use mecs::*;
use std::time::Instant;

pub const NUM_ENTITIES: usize = 1_000_000;

#[derive(Debug)]
pub struct Pos{
    x: f32,
    y: f32
}

pub struct Vel{
    x: f32,
    y: f32
}

#[derive(Debug)]
pub struct Name{
    name: String
}
impl Name{
    pub fn new(name: &str) -> Self{
        Name{
            name: name.to_string()
        }
    }
}

impl_ecs!(
    name: Ecs,
    entity: Entity{
        components: [
            pos: Pos,
            vel: Vel
        ],
        member: [
            
        ]
    }
);


pub fn physics(ecs: &mut Ecs, entities: &[Entity]){
    for e in entities{
        let (mut pos, vel) = ecs.get_mut(e);
        match (pos, vel){
            (Some(p), Some(v)) => {
                p.x += v.x;
                p.y += v.y;
            },
            _ =>{}
        }
    }
    
}

pub fn main(){
    let mut ecs = Ecs::new();

    let mut entities = Vec::with_capacity(NUM_ENTITIES);

    let build_start = Instant::now();

    for i in 0..(NUM_ENTITIES / 10 * 9){
        let e = ecs.new_entity();
        let (mut pos, _vel) = ecs.get_mut(&e);
        *pos = Some(Pos{x: 1.0, y: -2.5});
        entities.push(e);
    }

    for i in 0..(NUM_ENTITIES / 10 * 1){
        let e = ecs.new_entity();
        let (mut pos, mut vel) = ecs.get_mut(&e);
        *pos = Some(Pos{x: 1.0, y: -2.5});
        *vel = Some(Vel{x: 2.0, y: 0.5});
        entities.push(e);
    }

    let btime = build_start.elapsed();
    println!("Build Needed {}sec / {}msec / {}nsec",
             btime.as_secs(), btime.as_millis(), btime.as_nanos()
    );

    println!("Which is {}msec per inst", btime.as_millis() as f64 / 10_000.0);    
    return;
    loop{
        let upd_start = Instant::now();

        println!("=====");
        physics(&mut ecs, &entities);
        let time = upd_start.elapsed();

        println!("Needed {}sec / {}msec / {}nsec",
                 time.as_secs(), time.as_millis(), time.as_nanos()
        );

        println!("Which is {}msec / {}ns per inst", time.as_millis() as f64 / 10_000.0, time.as_nanos() as f64 / 10_000.0);    
    }
}
