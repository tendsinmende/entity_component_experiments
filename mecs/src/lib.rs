//! #Mecs
//! An macros based ecs system.
//!
//! On my quest to find the nicest version of creating entities with components in rust I
//! now try to employ a simple container system for resources as well as Entity traits
/*
#![feature(trace_macros)]

trace_macros!(true);
*/

pub mod container;
pub use container::*;


///Implements an ecs that can attach a given set component types to an entity.

pub trait AbstractEntity{
    type Data;
    fn pointer(&self) -> &DataPointer<Self::Data>;
}

#[macro_export]
macro_rules! impl_ecs{
    (
        name: $name:ident,
        entity: $entity_name:ident{
            components: [$(
                $component_name:ident : $component_type:ty
            ),*],
            member: [$(
                $member_name:ident : $member_type:ty
            ),*]
        }
    ) => (
        impl_container!(EcsContainer, $($component_name : $component_type),*);

        pub struct $entity_name{
            data: DataPointer<($($component_type),*)>,
            $(
                $member_name : $member_type
            ),*
        }

        impl AbstractEntity for $entity_name{
            type Data = ($($component_type),*);
            fn pointer(&self) -> &DataPointer<Self::Data>{
                &self.data
            }
        }

        //The ecs implementation
        pub struct $name{
            container: EcsContainer,
        }

        impl $name{
            pub fn new() -> Self{
                $name{
                    container: EcsContainer::new(),
                }
            }

            pub fn new_entity(&mut self, $($member_name : $member_type),*) -> $entity_name{
                $entity_name{
                    data: self.container.new_slot(),
                    $(
                        $member_name : $member_name
                    ),*
                }
            }

            pub fn get(&self, entity: &impl AbstractEntity<Data=($($component_type),*)>) -> ($(&Option<$component_type>),*){
                self.container.get(entity.pointer())
            }

            pub fn get_mut(&mut self, entity: &impl AbstractEntity<Data=($($component_type),*)>) -> ($(&mut Option<$component_type>),*){
                self.container.get_mut(entity.pointer())
            }

            pub fn get_container(&self) -> &EcsContainer{
                &self.container
            }

            pub fn get_container_mut(&mut self) -> &mut EcsContainer{
                &mut self.container
            }
        }
    )
}   


#[cfg(test)]
mod ecs_test{
    use crate::*;
    
    pub struct Vel{
        x: f32,
        y: f32
    }

    pub struct Pos{
        x: f32,
        y: f32
    }

    pub struct Name{
        str: String
    }
    
    impl_ecs!(
        name: Ecs,
        entity: Entity{
            components: [
                pos: Pos,
                vel: Vel,
                name: Name
            ],
            member: [
                description: String
            ]
        }
    );

    
    #[test]
    pub fn arbitary_ecs(){
        let mut ecs = Ecs::new();
        let test_entity = ecs.new_entity("NoiceTestDescription".to_owned());
    }
}


