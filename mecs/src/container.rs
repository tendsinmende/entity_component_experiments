
use std::marker::PhantomData;



///Points to a set of variables in a container, Should never be created by hand since this can crash a container.
pub struct DataPointer<T>{
    
        
    pub _marker: PhantomData<T>,
    pub location: usize
}

impl<T> DataPointer<T>{
    pub fn index(&self) -> usize{
        self.location
    }
}

///Implements a contianer of with the type `name`, that can provide a iterator over tubels
/// of the `types` fields.
#[macro_export]
macro_rules! impl_container{
    ($name:ident, $($fname:ident : $ftype:ty), *) => (
        
        

        
        //Impl a container for the tuble of the types we got.
        // also impl a retriver iterator for each type
        pub struct $name{
            //Add a field for each type
            $($fname: Vec<Option<$ftype>>,)*
            free_slots: Vec<usize>,
            next_index: usize
        }

        impl $name{
            pub fn new() -> Self{
                $name{
                    $($fname: Vec::new(),)*
                    free_slots: Vec::new(),
                    next_index: 0,
                }
            }

            //Implement iterators over each field
            $(
                pub fn $fname(&self) -> &[Option<$ftype>]{
                    self.$fname.as_slice()
                }
            )*

            //Reserves a new slot withing this container
            pub fn new_slot(&mut self) -> DataPointer<($($ftype),*)>{
                use std::marker::PhantomData;
                //Depending on the state, insert at a slot or push new one
                if self.free_slots.len() > 0{
                    let index = self.free_slots.pop().expect("Could not pop free slot!");

                    $(
                        self.$fname[index] = None;
                    )*;

                    DataPointer::<($($ftype),*)>{
                        _marker: PhantomData,
                        location: index
                    }
                    
                }else{
                    let index = self.next_index;
                    self.next_index += 1;
                    $(
                        self.$fname.push(None);
                    )*;

                    DataPointer::<($($ftype),*)>{
                        _marker: PhantomData,
                        location: index
                    }
                }
            }

            pub fn get(&self, pointer: &DataPointer<($($ftype), *)>) -> ($(&Option<$ftype>),*){
                (
                    $(
                        //expect should never happen since pointers are always created by the system.
                        &self.$fname[pointer.index()]
                    ),*
                )
            }

            pub fn get_mut(&mut self, pointer: &DataPointer<($($ftype), *)>) -> ($(&mut Option<$ftype>),*){
                (
                    $(
                        &mut self.$fname[pointer.index()]
                    ),*
                )
            }
        }
    )
}


#[cfg(test)]
mod test_container{
    use crate::*;

    pub struct Vel{
        x: f32,
        y: f32
    }

    pub struct Pos{
        x: f32,
        y: f32
    }

    impl_container!(TestContainer, vel: Vel, pos: Pos);

    #[test]
    fn container_test(){
        let mut cont = TestContainer::new();
        let ptr = cont.new_slot();
        let (mut vel, mut pos) = cont.get_mut(&ptr);
        *vel = Some(Vel{x: 11.0, y: -2.0});
    }
}
